'use strict';

module.exports = {
    entry: "./src/app",
    output: {
        filename: "bundle.js"
    },

    watch: true,

    watchOptions: {
        aggregateTimeout: 100
    },

    devtool: "source-map",

    module: {

        loaders: [{
            test: /\.js$/,
            exclude: /(node_modules|bower_components)/,
            loader: 'babel-loader',
            query: {
                presets: ['react','es2015']
            }
        }]

    }
};