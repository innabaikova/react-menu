import React, { Component, PropTypes } from 'react'
import ReactDOM from 'react-dom';
import Menu from '../src/components/menu'

class App extends Component {
    render() {
        return (
            <div className="app">
                <Menu />
            </div>
        );
    }
}

ReactDOM.render(
    <App />,
    document.getElementById('root')
);