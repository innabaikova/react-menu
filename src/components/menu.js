import React, { Component, PropTypes } from 'react'
import ReactDOM from 'react-dom';

class Menu extends Component {
    constructor() {
        super();
        this.state = {
            items: [
                "Campaign",
                "Payment",
                "Assigment",
                "Balance",
                "User",
                "Post",
                "Vip Application",
                "Statistics",
                "Influencers",
                "A Team"
            ].map((item, index) => {
                return {
                    name: item,
                    active: index == 0
                }
            })
        }
    }

    componentDidMount() {
        window.addEventListener("resize", this.onWindowResize.bind(this));
    }

    componentWillUnmount() {
        window.removeEventListener("resize", this.onWindowResize.bind(this));
    }

    moveSlidingMarker(offsetLeft, offsetTop) {

        let markerLeftOffset = offsetLeft - 32 + "px";
        let markerTopOffset = offsetTop - 14 + "px";

        let marker = document.getElementsByClassName("menu__sliding-marker")[0];
        marker.style.marginLeft = markerLeftOffset;
        marker.style.marginTop = markerTopOffset;

    }

    onMenuItemClick(e, index) {

        this.moveSlidingMarker(e.target.offsetLeft, e.target.offsetTop);

        setTimeout(() => {
            let activeItemIndex = this.state.items.findIndex(item => item.active);

            if (activeItemIndex != index) {
                const items = this.state.items.slice();
                items[activeItemIndex].active = false;
                items[index].active = true;
                this.setState({items: items});
            }
        }, 200);

    }

    onWindowResize() {
        let activeElement = document.getElementsByClassName("menu__item active")[0];
        this.moveSlidingMarker(activeElement.offsetLeft, activeElement.offsetTop);
    }

    render() {

        const items = this.state.items;

        const itemsTemplate = items.map((item, index) => {
            return (
                <li className={"menu__item " + (item.active ? "active" : "")} key={index}>
                    <a className="menu__item__link " onClick={(e) => this.onMenuItemClick(e,index)}>
                        {item.name}
                    </a>
                </li>
            )
        });

        return (
            <ul className="menu">
                {itemsTemplate}
                <li className="menu__sliding-marker"></li>
            </ul>
        )
    }
}

export default Menu
